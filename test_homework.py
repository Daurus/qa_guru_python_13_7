from pypdf import PdfReader
from openpyxl import load_workbook
import csv
import zipfile
from conftest import ZIP_RESOURCES


def test_csv():
    with zipfile.ZipFile(ZIP_RESOURCES) as zip_file:
        with zip_file.open('csv.csv') as csv_file:
            content = csv_file.read().decode('utf-8-sig')
            csvreader = list(csv.reader(content.splitlines()))
            second_row = csvreader[1]
            assert 'CN001' in second_row[0]
            assert 'iivanova@company.ru' in second_row[0]


def test_pdf():
    with zipfile.ZipFile(ZIP_RESOURCES) as zip_file:
        with zip_file.open('pdf.pdf') as pdf_file:
            reader = PdfReader(pdf_file)
            assert 'БОЛЬШАЯ КНИГА' in reader.pages[1].extract_text()

def test_xlsx():
    with zipfile.ZipFile(ZIP_RESOURCES) as zip_file:
        with zip_file.open('xlsx.xlsx') as xlsx_file:
            xlsx_reader = load_workbook(xlsx_file).active
            assert xlsx_reader.cell(row=3, column=2).value == 20240322