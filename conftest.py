import zipfile
import os.path
import shutil
import pytest


RESOURCES_PATH = os.path.join(os.getcwd(), "resources")
ZIP_PATH = os.path.join(os.getcwd(), "zip_resources")
ZIP_RESOURCES = os.path.join(os.getcwd(), 'zip_resources', 'zip_resources.zip')

#Создание папки с архивом, вложенные файлы xlsx.xlsx, pdf.pdf, csv.csv
@pytest.fixture(scope='function', autouse=True)
def create_archive():
    if not os.path.exists(ZIP_PATH):
        os.mkdir(ZIP_PATH)
    with zipfile.ZipFile(ZIP_PATH + '/zip_resources.zip', 'w') as zf:
        for file in os.listdir(RESOURCES_PATH):
            add_file = os.path.join(RESOURCES_PATH, file)
            zf.write(add_file, os.path.basename(add_file))
    yield
    shutil.rmtree(ZIP_PATH)
